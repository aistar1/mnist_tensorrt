
# https://cmake.org/cmake/help/latest/manual/cmake-modules.7.html


# OpenCV
# https://cmake.org/cmake/help/latest/module/FindOpenCV.html
# ----------------------------------------------------------------------------------
find_package(OpenCV REQUIRED)
message("OpenCV_VERSION: ${OpenCV_VERSION}")
message("OpenCV_INCLUDE_DIRS: ${OpenCV_INCLUDE_DIRS}")
message("OpenCV_LIBRARIES: ${OpenCV_LIBRARIES}")
message("----------------------------------------------------------------------------------")


# CUDA
# https://cmake.org/cmake/help/latest/module/FindCUDA.html
# ----------------------------------------------------------------------------------
find_package(CUDA REQUIRED)
message("CUDA_VERSION: ${CUDA_VERSION}")
message("CUDA_INCLUDE_DIRS: ${CUDA_INCLUDE_DIRS}")
message("CUDA_LIBRARIES: ${CUDA_LIBRARIES}")
message("----------------------------------------------------------------------------------")


