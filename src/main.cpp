#include "engine.h"
#include <opencv2/opencv.hpp>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;

int main(int argc, char** argv) {
    // Specify our GPU inference configuration options
    Options options;
    // TODO: If your model only supports a static batch size
    options.precision = Precision::FP16; // Use fp16 precision for faster inference.
    
    const std::string onnxModelpath = "../models/mnist.onnx";

    cv::Mat image = cv::imread("../inputs/img_1.jpg", cv::IMREAD_GRAYSCALE);
    cv::Mat img2;
    image.convertTo(img2, CV_32F);
    img2 = (img2 / 255 - 0.5) / 0.5;

    Engine engine(options);
    
    if (!engine.build(onnxModelpath)) {
        throw std::runtime_error("Unable to build TRT engine.");
    }

    if (!engine.loadNetwork()) {
        throw std::runtime_error("Unable to loadNetwork TRT engine.");
    }

    if (!engine.infer(img2)) {
        throw std::runtime_error("Unable to infer TRT engine.");
    }

    return 0;
}