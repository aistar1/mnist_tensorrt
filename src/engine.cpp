
#include <iostream>
#include <fstream>
#include <numeric>

#include "engine.h"
#include "NvOnnxParser.h"

using namespace nvinfer1;


void Logger::log(Severity severity, const char *msg) noexcept {
    // Would advise using a proper logging utility such as https://github.com/gabime/spdlog
    // For the sake of this tutorial, will just log to the console.

    // Only log Warnings or more important.
    if (severity <= Severity::kWARNING) {
        std::cout << msg << std::endl;
    }
}



Engine::Engine(const Options &options)
    : m_options(options)
{
}


Engine::~Engine() {
    // // Free the GPU memory
    // for (auto & buffer : m_buffers) {
    //     cudaError_t code = cudaFree(buffer);
    //     if (code != 0) {
    //         std::string errMsg = "CUDA operation failed with code: " + std::to_string(code) + "(" + cudaGetErrorName(code) + "), with message: " + cudaGetErrorString(code);
    //         std::cout << errMsg << std::endl;
    //         throw std::runtime_error(errMsg);
    //     }
    // }
    // m_buffers.clear();
}


bool Engine::doesFileExist(const std::string &filepath) {
    std::ifstream f(filepath.c_str());
    return f.good();
}

bool Engine::build(std::string onnxModelPath)
{

    if (doesFileExist(engineName)) {
        std::cout << "Engine found, not regenerating... " << engineName << std::endl;
        return true;
    }

    // Create our engine builder.
    auto builder = std::unique_ptr<nvinfer1::IBuilder>(nvinfer1::createInferBuilder(m_logger));
    // nvinfer1::IBuilder* builder = nvinfer1::createInferBuilder(m_logger);
    if (!builder) {
        return false;
    }

    // Define an explicit batch size and then create the network.
    // More info here: https://docs.nvidia.com/deeplearning/tensorrt/developer-guide/index.html#explicit-implicit-batch
    auto explicitBatch = 1U << static_cast<uint32_t>(NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);

    auto network = std::unique_ptr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(explicitBatch));
    if (!network) {
        return false;
    }

    // Create a parser for reading the onnx file.
    auto parser = std::unique_ptr<nvonnxparser::IParser>(nvonnxparser::createParser(*network, m_logger));
    if (!parser) {
        return false;
    }

    // We are going to first read the onnx file into memory, then pass that buffer to the parser.
    // Had our onnx model file been encrypted, this approach would allow us to first decrypt the buffer.

    std::ifstream file(onnxModelPath, std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);
    if (!file.read(buffer.data(), size)) {
        throw std::runtime_error("Unable to read engine file");
    }

    auto parsed = parser->parse(buffer.data(), buffer.size());
    if (!parsed) {
        return false;
    }

    auto config = std::unique_ptr<nvinfer1::IBuilderConfig>(builder->createBuilderConfig());
    if (!config) {
        return false;
    }

    if (m_options.precision == Precision::FP16) {
        config->setFlag(nvinfer1::BuilderFlag::kFP16);
    }

    // get network infomation
    getNetworkInfo(*network);

    // Build the engine
    std::unique_ptr<IHostMemory> plan{builder->buildSerializedNetwork(*network, *config)};
    if (!plan) {
        return false;
    }

    // Write the engine to disk
    std::ofstream outfile(engineName, std::ofstream::binary);
    outfile.write(reinterpret_cast<const char*>(plan->data()), plan->size());
    
    return true;
}


bool Engine::loadNetwork()
{
    // Read the serialized model from disk
    std::ifstream file(engineName, std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);
    if (!file.read(buffer.data(), size)) {
        throw std::runtime_error("Unable to read engine file");
    }

    std::unique_ptr<IRuntime> runtime{createInferRuntime(m_logger)};
    if (!runtime) {
        return false;
    }

    m_engine = std::unique_ptr<nvinfer1::ICudaEngine>(runtime->deserializeCudaEngine(buffer.data(), buffer.size()));
    if (!m_engine) {
        return false;
    }

    m_context = std::unique_ptr<nvinfer1::IExecutionContext>(m_engine->createExecutionContext());
    if (!m_context) {
        return false;
    }

    return true;
}

bool Engine::infer(const cv::Mat &input)
{
    // show input/output dims
    int nbBindings = m_engine->getNbBindings();
    for (int i = 0; i < nbBindings; ++i) {
        nvinfer1::Dims dims = m_engine->getBindingDimensions(i);
        std::string dims_string = dimsToString(dims);
        std::cout << "binding " << i << ": " << dims_string  << std::endl;
    }


    // get input len, like     nBatchSize * inputC * inputH * inputW * sizeof(float)
    const std::size_t inputLen = getLen(0);
    // get output len, like    nBatchSize * nOutputSize * sizeof(float)
    const std::size_t outputLen = getLen(1);

    // cereate GPU input/output buffer
    void* buffers[2] = { NULL, NULL };
    cudaMalloc(&buffers[0], inputLen);
    cudaMalloc(&buffers[1], outputLen);

    // create cuda stream
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    void *data = malloc(inputLen);
    memcpy(data, input.ptr<float>(0), inputLen);

    // host to GPU
    cudaMemcpyAsync(buffers[0], data, \
    inputLen, cudaMemcpyHostToDevice, stream);
    std::cout << "start to infer image..." << std::endl;

    // infernece
    bool status = m_context->enqueueV2(buffers, stream, nullptr);
    if (!status) {
        return false;
    }
    
    // GPU to host
    const nvinfer1::Dims output_dims = m_engine->getBindingDimensions(1);
	const std::size_t volume_sz  = volume(output_dims);
    float prob[volume_sz];
    cudaMemcpyAsync(prob, buffers[1], outputLen, cudaMemcpyDeviceToHost, stream);

    // Synch finish, free
    cudaStreamSynchronize(stream);
    cudaStreamDestroy(stream);

    std::cout << "image inference finished!  " << std::endl;
    cv::Mat result = cv::Mat(1, volume_sz, CV_32F, (float*)prob);
    float max = result.at<float>(0, 0);
    int index = 0;
    for (int i = 0; i < volume_sz; i++)
    {
        if (max < result.at<float>(0,i)) {
            max = result.at<float>(0, i);
            index = i;
        }
    }

    std::cout << "predict digit: " << index << std::endl;
    return true;
}


const std::size_t Engine::getLen(int num) 
{
    const nvinfer1::Dims output_dims = m_engine->getBindingDimensions(num);
	const std::size_t dims_sz  = volume(output_dims);
	const nvinfer1::DataType dtype  = m_engine->getBindingDataType(num);
	const std::size_t dtype_sz = getDtypeSize(dtype);
	const std::size_t alloc_sz = dims_sz * dtype_sz;
    return alloc_sz;
}

const std::size_t Engine::volume(const nvinfer1::Dims& d) {
	return std::accumulate(d.d, d.d + d.nbDims, 1, std::multiplies<int64_t>());
}

inline int Engine::getDtypeSize(nvinfer1::DataType trtDtype)
{
    // https://github.com/onnx/onnx-tensorrt/blob/main/trt_utils.hpp#L19
    switch (trtDtype)
    {
    case nvinfer1::DataType::kINT32: return 4;
    case nvinfer1::DataType::kFLOAT: return 4;
    case nvinfer1::DataType::kHALF:  return 2;
    case nvinfer1::DataType::kINT8:  return 1;
    }
    throw std::runtime_error("Invalid trtDtype.");
}



std::string Engine::dimsToString(const nvinfer1::Dims& dims)
{
	std::string msg;
    std::string temp_string;

	msg.reserve(1024);
	for (int idx = 0; idx < dims.nbDims; ++idx) {
		if (idx == 0) {
            temp_string = "[" + std::to_string(dims.d[idx]);
			msg.append(temp_string);
		} else {
            temp_string = ", " + std::to_string(dims.d[idx]);
			msg.append(temp_string);
		}
	}
	msg.append("]");
	return std::move(msg);
}

void Engine::getNetworkInfo(nvinfer1::INetworkDefinition& network)
{
	// show all inputs in the network
	for (int in_idx = 0; in_idx < network.getNbInputs(); ++in_idx) {
		nvinfer1::ITensor* tensor = network.getInput(in_idx);
		printf("  Network-Input#%d  <%s>  <%s> \n" , in_idx, 
        tensor->getName(), 
        dimsToString(tensor->getDimensions()).c_str());
	}
    printf("\n");
    
    // show all outputs in the network
    for (int out_idx = 0; out_idx < network.getNbOutputs(); ++out_idx) {
        nvinfer1::ITensor* tensor = network.getOutput(out_idx);
        printf("  Network-Output#%d  <%s>  <%s> \n" , out_idx, 
        tensor->getName(), 
        dimsToString(tensor->getDimensions()).c_str());
    }
    printf("\n");
}