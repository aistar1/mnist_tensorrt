

#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudaarithm.hpp>

#include "NvInfer.h"



// Precision used for GPU inference
enum class Precision {
    FP32,
    FP16
};


// Options for the network
struct Options {
    // Precision to use for GPU inference. 16 bit is faster but may reduce accuracy.
    Precision precision = Precision::FP16;

};


// Class to extend TensorRT logger
class Logger : public nvinfer1::ILogger {
    void log (Severity severity, const char* msg) noexcept override;
};



class Engine
{
public:
    Engine(const Options& options);
    ~Engine();
    // Build the network
    bool build(std::string onnxModelPath);
    // Load and prepare the network for inference
    bool loadNetwork();
    bool infer(const cv::Mat &input);

private:
    Options m_options;

    Logger m_logger;

    bool doesFileExist(const std::string& filepath);
    
    std::unique_ptr<nvinfer1::ICudaEngine> m_engine = nullptr;
    std::unique_ptr<nvinfer1::IExecutionContext> m_context = nullptr;
    std::string engineName = "trt.engine";
    std::vector<void*> m_buffers;


    std::string dimsToString(const nvinfer1::Dims&);
    void getNetworkInfo(nvinfer1::INetworkDefinition&);
    const std::size_t volume(const nvinfer1::Dims& d);
    inline int getDtypeSize(nvinfer1::DataType trtDtype);
    const std::size_t getLen(int num);


}; // Engine

