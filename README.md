# mnist_tensorrt



## Description
This project demonstrates how to use the TensorRT C++ API for high performance GPU inference.


## Prerequisites
- Install OpenCV with cuda support. Instructions can be found [here](https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7)
- Download TensorRT from [here](https://developer.nvidia.com/nvidia-tensorrt-8x-download)
- Extract, and then navigate to the `CMakeLists.txt` file and replace the `TODO` with the path to your TensorRT installation


## Usage

```
mkdir -p build
cd build
cmake ..
make -j4
./mnist
```

## result

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://www.kaggle.com/datasets/scolianni/mnistasjpg">
    <img width="20%" src="inputs/img_1.jpg" alt="logo">
  </a>

  <h3 align="center">net input: (1, 28, 28, 1)</h3>
  <h3 align="center">predict digit: 2</h3>
<br /> <!-- new line -->
<br /> <!-- new line -->

</p>

## References
[TensorRT](https://github.com/NVIDIA/TensorRT)  
[TensorRT C++ API Tutorial](https://github.com/cyrusbehr/tensorrt-cpp-api)
[]()  


